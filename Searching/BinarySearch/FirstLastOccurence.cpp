//This program is to find the first and last occurence of a number in a sorted array
//Time Complexity: O(logn)
//Space Complexity: O(1)

#include<bits/stdc++.h>
using namespace std;

vector<int> FirstLastOccurence(vector<int>& nums, int target) {
      int  low = 0, mid, high = nums.size() - 1, first = -1, last = -1, marker = -1, flag = true; 

    //For first occurence
      while(low <= high) {
          mid = low + (high - low)/2;
          if(nums[mid] == target)  { 
            first = mid;
            if(flag) marker = mid - 1, flag = false; //To mark the first apperance for second iteration
            high = mid - 1; }//Continue searching in left half
          else if(nums[mid] > target) high = mid - 1; 
          else low = mid + 1;
      } 

    //For last occurence
      low = marker, high = nums.size() - 1;
      while(low <= high) {
          mid = low + (high - low)/2;
          if(nums[mid] == target)  { 
            last = mid;
            low = mid + 1; }//Continue searching in right half
          else if(nums[mid] > target) high = mid - 1;
          else low = mid + 1;
          
      } 
      return {first, last};
}


int main() 
{
    vector<int> nums = {1,2,3,4,4,5,6,7,8,9};
    int target = 7;
    vector<int> test = FirstLastOccurence(nums, target);
    for(int i = 0; i < test.size(); i++) {
        cout<<test[i]<<" ";
    }
    return 0;
}

