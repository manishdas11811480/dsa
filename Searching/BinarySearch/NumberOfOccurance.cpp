//This program is to find the number of occurance of a number in a sorted array
//Time Complexity: O(logn)
//Space Complexity: O(1)

#include<bits/stdc++.h>
using namespace std;

int findOccurance(vector<int>& nums, int target) {
    int  low = 0, mid, high = nums.size() - 1, first = -1, last = -1, marker = -1, flag = true; 

    //For first occurence
      while(low <= high) {
          mid = low + (high - low)/2;
          if(nums[mid] == target)  { 
            first = mid;
            if(flag) marker = mid , flag = false;
            high = mid - 1; }//Continue searching in left half
          else if(nums[mid] > target) high = mid - 1; 
          else low = mid + 1;
      } 

    //For last occurence
      low = marker, high = nums.size() - 1;

      while(low <= high) {  
          mid = low + (high - low)/2;
          if(nums[mid] == target)  { 
            last = mid;
            //cout<<res<<endl;
            low = mid + 1; }//Continue searching in right half
          else if(nums[mid] > target) high = mid - 1;
          else low = mid + 1;  
      } 
      //cout<<"first: "<<first<<" last: "<<last<<endl;
      return last - first + 1; //Array is sorted so number of ocurrence will be first - last occurence + 1
}


int main() 
{
    vector<int> nums = {0,1,2,3,4,4,5,6,6,6,6,8,8,8,8,9,9,9}; 
    int target = 6;
    cout<<findOccurance(nums, target);
    return 0;
}


