#include<bits/stdc++.h>
using namespace std;

int search(vector<int>& nums, int target) {
        int low = 0, mid, high = nums.size() - 1;
        int asc = false;

        if(nums.size() == 1 && nums[0] == target) return 0;
        else if(nums.size() == 1 && nums[0] != target) return -1;
        else if(nums[low] < nums[low+1]) asc = true;

        while(low<=high) {

           mid = low + (high - low)/2; //mid = (low + high) / 2 will cause overflow but works in most of cases

            if(nums[mid] == target) return mid;
            
            else if(nums[mid] < target) {
              if(asc) low = mid + 1;
              else high = mid - 1;
            }

            else if(nums[mid] > target) {
              if(asc) high = mid - 1;
              else low = mid + 1;
            } 
        }
        return -1;
}

int main() 
{
    vector<int> revNums = {11,9,7,5,4,3,2,1}; //Reverse Sorted
    vector<int> nums = {1,2,3,4,5,7,9,11}; //Reverse Sorted

    int target = 3;
    cout << search(nums, target) << endl;   
    return 0;
}

