/*
Author: Manish Das
Date: 7th July, 2023
Purpose: Queue implementation using Array
*/

#include <bits/stdc++.h>
using namespace std;

#define MAX 2
int myQueue[MAX];
int front = -1, rear = -1;

void enqueue(int n)
{
    if (rear == MAX - 1)
    {
        cout << "Overflow!" << endl;
    }
    else
    {
        if (front == -1)
            front = 0;
        rear++;
        myQueue[rear] = n;
        cout << "Element " << n << " inserted successfully" << endl;
    }
}

void dequeue()
{
    if (front && rear == -1)
    {
        cout << "Queue is empty!" << endl;
    }
    else if (front > rear)
    {
        cout << "Underflow" << endl;
    }
    else
    {
        cout << "Element " << myQueue[front++] << " removed successfully" << endl;
    }
}

int displayFront()
{
    if (front != -1)
    {
        return myQueue[front];
    }
    else
    {
        return -1;
    }
}

int displayRear()
{
    if (rear != -1)
    {
        return myQueue[rear];
    }
    else
    {
        return -1;
    }
}

bool isFull()
{
    if (rear == MAX - 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isEmpty()
{
    if (front && rear == -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void display()
{
    if (front && rear == -1)
    {
        cout << "Queue is empty!" << endl;
    }
    else
    {
        for (int i = front; i <= rear; i++)
        {
            cout << myQueue[i] << " ";
        }
        cout << endl;
    }
}

int main()
{
    enqueue(23);
    enqueue(98);
    // enqueue(28);
    // enqueue(17);
    enqueue(48);

    cout << "Front: " << front << "  Rear: " << rear << endl;

    dequeue();
    dequeue();
    dequeue();

    display();

    // cout<<isFull()<<endl;
    // cout<<isEmpty()<<endl;

    return 0;
}