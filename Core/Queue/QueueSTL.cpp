/*
Author: Manish Das
Date: 7th July, 2023
Purpose: Queue implementation using STL
*/

#include <bits/stdc++.h>
using namespace std;

int main() {
    queue<int> q;

    q.push(2);
    q.push(38);
    q.push(65);
    q.push(89);
    q.push(49);

    q.pop();

    cout<<q.front()<<endl;
    cout<<q.back()<<endl;

    while(!q.empty()) {
        cout<<q.front()<<" ";
        q.pop();
    }
    cout<<endl;
}