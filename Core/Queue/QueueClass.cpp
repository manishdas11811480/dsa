/*
Author: Manish Das
Date: 7th July, 2023
Purpose: Queue implementation using Array and Class
*/

#include <bits/stdc++.h>
using namespace std;

#define MAX 2

class Queue
{
    int myQueue[MAX];
    int front = -1, rear = -1;

public:
    void enqueue(int n)
    {
        if (rear == MAX - 1)
        {
            cout << "Overflow!" << endl;
        }
        else
        {
            if (front == -1)
                front = 0;
            rear++;
            myQueue[rear] = n;
            cout << "Element " << n << " inserted successfully" << endl;
        }
    }

    void dequeue()
    {
        if (front && rear == -1)
        {
            cout << "Queue is empty!" << endl;
        }
        else if (front > rear)
        {
            cout << "Underflow" << endl;
        }
        else
        {
            cout << "Element " << myQueue[front++] << " removed successfully" << endl;
        }
    }

    int displayFront()
    {
        if (front != -1)
        {
            return myQueue[front];
        }
        else
        {
            return -1;
        }
    }

    int displayRear()
    {
        if (rear != -1)
        {
            return myQueue[rear];
        }
        else
        {
            return -1;
        }
    }

    bool isFull()
    {
        if (rear == MAX - 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool isEmpty()
    {
        if (front && rear == -1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void display();
};

void Queue::display()
{
    if (front && rear == -1)
    {
        cout << "Queue is empty!" << endl;
    }
    else
    {
        for (int i = front; i <= rear; i++)
        {
            cout << myQueue[i] << " ";
        }
        cout << endl;
    }
}

int main()
{
    Queue q;
    q.enqueue(23);
    q.enqueue(98);
    // enqueue(28);
    // enqueue(17);
    q.enqueue(48);

    q.dequeue();
    q.dequeue();
    q.dequeue();

    q.display();

    // cout<<isFull()<<endl;
    // cout<<isEmpty()<<endl;

    return 0;
}