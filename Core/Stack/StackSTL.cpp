/*
Author: Manish Das
Date: 4th July, 2023
Purpose: Stack implementation using STL
*/

#include <bits/stdc++.h>
using namespace std;

int main()
{
    stack<int> s;
    s.push(12);
    s.push(23);
    s.push(19);
    s.push(76);
    s.push(49);

    s.pop();

    while (!s.empty())
    {
        cout << s.top() << " ";
        s.pop();
    }
    return 0;
}