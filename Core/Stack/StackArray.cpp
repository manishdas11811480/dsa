/*
Author: Manish Das
Date: 4th July, 2023
Purpose: Stack implementation using array
*/

#include <bits/stdc++.h>
using namespace std;

#define MAX 10
int myStack[MAX];

int top = -1;

void push(int n)
{
    if (top == MAX - 1)
    {
        cout << "Stack is already full!" << endl;
    }
    else
    {
        top++;
        myStack[top] = n;
        cout << "Pushed element '" << n << "' successfully" << endl;
    }
}

void pop()
{
    if (top == -1)
    {
        cout << "Stack is already empty!" << endl;
    }
    else
    {
        int poppedElement = myStack[top--];
        cout << "Popped element is: " << poppedElement << endl;
    }
}

int peekAtPos(int n)
{
    if (n >= MAX)
    {
        cout << "Please pass value between the MAX Range" << endl;
        return -1;
    }
    else if (n < MAX)
    {
        return myStack[n];
    }
    else
    {
        return -1;
    }
}

int peek()
{
    if (top < 0)
    {
        cout << "Stack is empty" << endl;
        return -1;
    }
    return myStack[top];
}

int isFull()
{
    return (top == MAX - 1); // will return 1 if stack is empty
}

int isEmpty()
{
    return (top < 0); // will return 1 if stack is empty
}

void display()
{
    int tempTop = top; //modifying original top value will remove actual elements from the stack
    while (tempTop != -1)
    {
        cout << myStack[tempTop--] << endl;
    }
}

int main()
{
    // pushing elements into the stack
    push(5);
    push(12);
    push(3);
    push(9);
    push(18);
    push(21);

    display();

    // removing elements from the stack
    pop();
    pop();

    cout << "Top element of the stack is: " << peek() << endl;
    cout << "Is the stack empty? " << (isEmpty() ? "Yes" : "No") << "\n";
    cout << "Is the stack full? " << (isFull() ? "Yes" : "No") << "\n";
    return 0;
}