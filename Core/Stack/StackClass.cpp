/*
Author: Manish Das
Date: 4th July, 2023
Purpose: Stack implementation using class and array
*/

#include <iostream>

#define MAX_SIZE 100

class Stack {
private:
    int arr[MAX_SIZE];
    int top;

public:
    Stack() {
        top = -1; //Initialize stack top to -1 using constructor its an better apporach
    }

    void push(int element) {
        if (top >= MAX_SIZE - 1) {
            std::cout << "Stack Overflow\n";
        } else {
            arr[++top] = element;
            std::cout << element << " pushed to stack\n";
        }
    }

    void pop() {
        if (top < 0) {
            std::cout << "Stack Underflow\n";
        } else {
            int poppedElement = arr[top--];
            std::cout << poppedElement << " popped from stack\n";
        }
    }

    int peek() {
        if (top < 0) {
            std::cout << "Stack is empty\n";
            return -1;
        }
        return arr[top];
    }

    bool isEmpty() {
        return (top < 0);
    }
};

int main() {
    Stack stack;

    stack.push(10);
    stack.push(20);
    stack.push(30);
    stack.push(40);

    std::cout << "Top element of the stack: " << stack.peek() << "\n";

    stack.pop();
    stack.pop();

    std::cout << "Top element of the stack: " << stack.peek() << "\n";

    std::cout << "Is the stack empty? " << (stack.isEmpty() ? "Yes" : "No") << "\n";

    return 0;
}
