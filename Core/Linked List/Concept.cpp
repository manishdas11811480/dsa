/*
Author: Manish Das
Date: 8th July, 2023
Purpose: Linked List Basic Structure
*/

#include <bits/stdc++.h>
using namespace std;

class Node {
    public:
    int data;
    Node * next;
};

int main() {
    Node * head = new Node();
    Node * first = new Node();
    Node * second =  new Node();
    Node * third =  new Node();
    
    head->next = first;
    first->next = second;
    second->next = third;
    third->next = NULL;

    first->data = 12;
    second->data = 38;
    third->data = 60;

    cout<<first->next->data<<endl;
    cout<<first->next->next->data<<endl;

    return 0;
}