/*
Author: Manish Das
Date: 8th July, 2023
Purpose: Linked List Basic Operations
*/

#include <bits/stdc++.h>
using namespace std;

class Node
{
public:
    int data;
    Node *next;
};

Node *head = NULL;

void print()
{
    Node *temp = head;

    while (temp != NULL)
    {
        cout << temp->data << endl;
        temp = temp->next;
    }
}

void insertAtFront(int n)
{

    Node *temp = new Node();
    temp->data = n;

    temp->next = head;
    head = temp;
}

void deleteFromFront()
{
    if (head == NULL)
    {
        cout << "Linked list is empty!" << endl;
    }
    else
    {
        Node *temp = head;
        head = head->next;
        delete temp; // or free(temp) to Make sure to free unused memory
    }
}

void insertAtEnd(int n)
{
    Node *temp = head;

    if (head == NULL)
    {
        cout << "Linked List is empty!" << endl;
    }
    else
    {
        Node *newlyCreated = new Node();
        newlyCreated->data = n;
        newlyCreated->next = NULL;

        if (head->next == NULL)
        {
            head->next = newlyCreated;
        }

        else
        {
            while (temp->next != NULL)
            {
                temp = temp->next;
            }
            temp->next = newlyCreated;
        }
    }
}

void deleteFromEnd()
{
    Node *temp = head;

    if (head == NULL)
    {
        cout << "Linked List is empty!" << endl;
    }
    else if (head->next == NULL)
    {
        free(temp);
        cout << "List is empty now!" << endl;
    }
    else
    {
        while (temp->next->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = NULL;

        cout << "Temp: " << temp->data << endl;

        free(temp->next);
    }
}

void insertAtPos(int pos, int n)
{
    Node *temp = head;
    int count = 0;
    int elements = 0;

    while (temp != NULL)
    {
        temp = temp->next;
        elements++;
    }

    if (pos > elements)
    {
        cout << "Selected position is out of index!" << endl;
    }
    else
    {
        Node *newNode = new Node();
        newNode->data = n;


        temp = head;

        if (pos == 1)
        {
            newNode->next = head->next;
            head = newNode;
        }
        else
        {

            while (count != (pos - 2))
            {
                temp = temp->next;
                count++;
            }

            // Node *deletedNode = temp->next;
            // temp->next = temp->next->next;
            // free(deletedNode);
            // cout<<"Data: "<<temp->data<<endl;
            newNode->next = temp->next;
            temp->next = newNode;
        }
    }
}

void deleteFromPos(int pos)
{
    Node *temp = head;
    int count = 0;
    int elements = 0;

    while (temp != NULL)
    {
        temp = temp->next;
        elements++;
    }

    if (pos > elements)
    {
        cout << "Selected position is out of index!" << endl;
    }
    else
    {
        temp = head;

        if (pos == 1)
        {
            head = head->next;
            free(temp);
        }
        else
        {

            while (count != (pos - 2))
            {
                temp = temp->next;
                count++;
            }

            Node *deletedNode = temp->next;
            temp->next = temp->next->next;
            free(deletedNode);
            // cout<<temp->data<<endl;
        }
    }
}

void deleteFromMiddle()
{
    if (head == NULL)
    {
        cout << "List is empty!" << endl;
    }
    else
    {
        Node *temp = head;

        int count = 0;
        while (temp->next != NULL)
        {
            temp = temp->next;
            count++;
        }

        // cout<<"Count: "<<count<<endl; //if odd then middle is available

        if (count % 2 == 0)
        {
            int middle = count / 2;
            cout << "Middle: " << middle << endl;
            temp = head;
            count = 0;

            while (count != middle - 1)
            {
                temp = temp->next;
                count++;
            }
            cout << "Test: " << temp->data << endl;

            Node *deletedNode = temp->next;
            temp->next = temp->next->next;
            free(deletedNode); // Relesing the memory
        }
        else
        {
            cout << "Middle Not available" << endl;
        }
    }
}

void insertAtMiddle(int n)
{
    if (head == NULL)
    {
        cout << "List is empty!" << endl;
    }
    else
    {
        Node *temp = head;
        int count = 0;
        while (temp->next != NULL)
        {
            temp = temp->next;
            count++;
        }

        // cout<<"Count: "<<count<<endl; //if odd then middle is available

        if (count % 2 != 0)
        {
            int middle = count / 2;
            // cout << "Middle: " << middle << endl;
            temp = head;
            count = 0;

            while (count != middle)
            {
                temp = temp->next;
                count++;
            }
            Node *nNode = new Node();
            nNode->data = n;
            nNode->next = temp->next;
            temp->next = nNode;

            //  << "Test: " << temp->data << endl;
        }
        else
        {
            cout << "Middle Not available" << endl;
        }
    }
}

int main()
{
    insertAtFront(3);
    insertAtFront(8);
    insertAtFront(1);
    insertAtFront(67);
    insertAtFront(90);
    insertAtFront(97);
    insertAtFront(93);

    // deleteFromFront();

    // deleteFromEnd();

    // insertAtEnd(66);

    // insertAtMiddle(44);
    // deleteFromMiddle();
    //insertAtPos(7,100);
    // deleteFromPos(1);

    print();
    return 0;
}
