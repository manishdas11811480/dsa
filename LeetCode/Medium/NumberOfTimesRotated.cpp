//Program for finding the number of times a sorted array is rotated
//Time Complexity: O(logn)
//Space Complexity: O(1)

#include<bits/stdc++.h>
using namespace std;

int findRotation(vector<int>& nums) {
    int low = 0, high = nums.size() - 1, mid, res = -1;

    while(low<=high) {
        mid = low + (high - low) / 2;

        if(nums[mid] < nums[(mid + 1) % nums.size() - 1]  &&  nums[(mid + (nums.size() - 1) - 1) % nums.size() - 1] < nums[mid - 1]) {
            cout<<"Hello"<<endl;
            res = mid;
            break;
        }
        else if(nums[mid] > nums[low]) {
            low = mid + 1;
            cout<<"Right"<<endl;
        }
        else if(nums[mid] < nums[low]) {
            high = mid - 1;
            cout<<"Left"<<endl;
        }
    }
    return res;
}
 
int main() {
    vector<int> nums = {11,12,15,18,2,5,6,8};
    cout<<"Array Rotated: "<<findRotation(nums)<<" Times" <<endl;
    return 0;
}