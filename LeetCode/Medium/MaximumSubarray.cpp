class Solution {
public:
    int maxSubArray(vector<int>& nums) {
       int sum = 0;
       int maxSum = nums[0];

       for(int i=0;i<nums.size();i++) {
            sum += nums[i];
            maxSum = max(sum, maxSum); //compare which one is larger

            if(sum < 0) { //if sum is less than 0 that part dosen't need to include
                sum = 0;
            }   
       } 
       return maxSum;
    }
}; 