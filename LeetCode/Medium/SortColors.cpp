//Write a code for sorting the colors in an array using c++
//Time Complexity: O(n)
//Space Complexity: O(1)

//Counting approach- Require two passes
class Solution {
public:
    void sortColors(vector<int>& nums) {
        int count0 = 0, count1 = 0, count2 = 0;

        for(int i=0;i<nums.size();i++) {
            if(nums[i] == 0) {
                count0++;
            }
            else if(nums[i] == 1) {
                count1++;
            }
            else {
                count2++;
            }
        }

        int i = 0;

        while(count0 > 0) {
            nums[i++] = 0;
            count0--;
        }

        while(count1 > 0) {
            nums[i++] = 1;
            count1--;
        }

        while(count2 > 0) {
            nums[i++] = 2;
            count2--;
        }
    }
};

//Three pointer approach - Require one pass
//Time Complexity: O(n)
//Space Complexity: O(1)
//For More detail = https://www.youtube.com/watch?v=oaVa-9wmpns

class Solution {
public:
    void sortColors(vector<int>& nums) {

        int low = 0, mid = 0, high = nums.size() - 1;

        for(int i=0;i<nums.size();i++) {
            if(nums[mid] == 0) {
                swap(nums[low], nums[mid]); 
                low++;
                mid++;
            }
            else if(nums[mid] == 1) {
                mid++;  
            }
            else if(nums[mid] == 2) {
                swap( nums[mid], nums[high]);
                high--;
            }
        }

        
    }
};