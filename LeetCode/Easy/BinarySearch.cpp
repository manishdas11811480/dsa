//If any question has sorted keyword it might be solved using binary search
class Solution {
public:
    int search(vector<int>& nums, int target) {
        int low = 0, mid, high = nums.size() - 1;

        while(low<=high) {

           mid = low + (high - low)/2; //mid = (low + high) / 2 will cause overflow but works in most of cases

            if(nums[mid] == target) return mid;

            else if(nums[mid] < target) {
              low = mid + 1;
            }
            else if(nums[mid] > target) {
                high = mid - 1;
            }
        }
        return -1;
    }
};