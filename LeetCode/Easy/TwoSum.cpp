/* 
First Approach
Two sum problem
Using 0(n) time complexity and 0(n) space complexity
Hashing is used to solve this problem
*/

class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        map<int, int> mp;
       
        for(int i = 0; i < nums.size(); i++){
            if(mp.count(target - nums[i]) == 0) //If the element is not present in the map //if(mp.find(target - nums[i]) == mp.end()) this syntax also works more at the end of this file
                mp[nums[i]] = i;
            else
                return {mp[target - nums[i]], i}; //If the element is present in the map simply retrun the index
        }
        return {-1};
    }
};






/* 
Same Approach but with unordered_map
Two sum problem
Using 0(n) time complexity and 0(n) space complexity
Hashing is used to solve this problem
*/

class Solution {
public:
//easy solution using unordered map
    vector<int> twoSum(vector<int>& nums, int target) {
    unordered_map<int,int>ump;
        for(int i=0;i<nums.size();i++)
        {
            auto itr= ump.find(target-nums[i]);
            if(itr!=ump.end()){
                return {itr->second,i};
            }
            else ump.insert({nums[i],i});
        }
      return {};
    
        
    }
};



/* 
If the element is not found in the map, the iterator returned will be equal to m.end(), 
which is an iterator that points to one past the last element in the map.
*/