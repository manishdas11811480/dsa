#include <bits/stdc++.h>
using namespace std;

int main() {
    int arr[] = {1, 2, 3, 3, 4, 4, 5, 6, 7, 8, 9, 10, 10};
    

    int n = sizeof(arr) / sizeof(arr[0]);
    int newArr[n];

    int counter = 0;

    for(int i = 0; i < n; i++){
        if(arr[i] != arr[i+1]){
            newArr[counter] = arr[i];
            counter++;
        }
    }

    return 0;
}
